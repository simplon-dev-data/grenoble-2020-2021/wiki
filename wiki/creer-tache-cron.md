# Comment créer une tâche cron ?  

On cherche à automatiser une tâche à un moment défini ? **Cron** est la solution.  

Afin d'ouvrir **cron** il suffit de se placer dans son terminal et de taper la commande: 

### *crontab -e* 
 cette commande va nous permettre d'éditer cron et d'accéder à la page d'édition de **cron**  
 une fois dans la page d'édition il nous suffit de choisir :  

 \* * * * * où :

 * \* la minute
 * \* l'heure  
 * \* le jour (dans le mois)  
 * \* le mois 
 * \* le jour dans la semaine 
 *  et la tache à éxecuter. 

 par exemple si on veut être notifié d'un anniversaire tous les 26 avril à 5h du matin :  

 0 5 26 4 * echo "C'est l'anniversaire de Théo aujourd'hui !" >>~/readme 

 voilà vous savez créer une tâche **cron** maintenant, Congrats !! 