# Comment sélectionner des colonnes dans une base de données SQLite ?

`
SELECT
` est la commande qui permet de sélectionner des colonnes dans SQLite.  
Il est nécessaire, au minimum, de spécifier la table dans laquelle on va sélectionner des colonnes, en utilisant la commande `FROM`.

e.g.:
```SQL
SELECT * FROM <table> ;
```

## Sélectionner toutes les colonnes de la table

On peut utiliser `*` comme dans l'exemple au dessus pour sélectionner **toutes** les colonnes.

## Colonnes individuelles
Autrement, il faut écrire le nom de chaque colonne que l'on veut sélectionner, dans l'ordre dans lequel on souhaite les voir apparaître, séparées par une virgule à chaque fois.

e.g.:
```SQL
SELECT colonne1, colonne2, colonneN FROM <table> ;
```

## Renommer une colonne sélectionnée

Il est également possible de renommer une ou des colonne(s) sélectionnée(s) en utilisant la commande `AS`.

e.g.:
```SQL
SELECT colonne1 AS alias1, colonneN AS aliasN FROM <table> ;
```

Cet exemple sélectionnerait la "*colonne1*" et la "*colonneN*" et les renommerait "*alias1*" et "*aliasN*".

## Autres infos

Ce [**LIEN**](https://sqlite.org/lang_select.html) contient des infos sur l'ordre des commandes exécutées par SQLite lors d'une sélection.  
Cela concerne plutôt la sélection des lignes que celle des colonnes, je ne m'y attarderai donc pas.