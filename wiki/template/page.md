# Page Template

> [Phrase d'accroche]

!!! tip "TL;DR"
    [Résumé de la page]

## Problème

[Explication du problème]

## Solution

[Explication de la solution, avec illustrations et références à l'appui]

[Exemple de code Bash]

```bash
echo "Welcome to data!"
```

[Exemple de code Python]

```python
def say_hi(name):
    return f"Hi, {name}!"
```

[Exemple de code SQL]

```sql
SELECT first_name, last_name
FROM students
WHERE formation == 'Simplon Dev Data'
```

## Références

[Liens vers les références externes utilisées et autres informations utiles]
