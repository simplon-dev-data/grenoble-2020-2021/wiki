# Comment fonctionne le langage Markdown ?

Markdown est un langage de balisage que vous pouvez utiliser pour ajouter des éléments de mise en forme à des documents en texte brut.

Son but est d'offrir une syntaxe facile à lire et à écrire. Un document balisé par Markdown peut être lu en l'état sans donner l’impression d'avoir été balisé ou formaté par des instructions particulières.

## TLDR

Cette article donne des principaux petits idees pour améliorer le style et la structure d'un document Markdown. Dans les références, vous pouvez trouver des informations plus détaillées. 

## Problème

Comment faire pour mettre la mise en forme à documents? Comment par example: 
1. _Titres_ 
2. _Saute de lignes_
3. _L’emphase_
4. _Listes_
5. _Les tableaux_
6. _Inserer liens_
7. _Inserer images_
8. _Des schémas graphiques_
   
## 1 - Titres

Il y a trois options.

*Opcion 1. 

En ajoutant le symbol dièse (#) précedent le texte.

Syntax markdown:
```md          
# Titre 1  
## Titre2
### Titre3
```  
Visuellement:
# Titre 1  
## Titre 2
### Titre 3

Plus il y a de dièses, plus la taille de la lettre sera petite.

*Option 2.

La méthode par soulignement permet deux niveaux de titre. Il faut souligner votre ligne de texte par deux caractères ou plus -sans limite supérieure. Les = pour un titre 1 et les - pour un titre 2.

Syntax markdown:
```md          
Title 1
==
Title 2
-
```  

Visuellement:

Title 1
==
Title 2
-

*Option 3. 

Le format Export HTMLé des titres est hx où x est le niveau de titre. Les titres de niveau 1 sont les plus importants, et l’importance décroît avec le nombre.

Syntax markdown:

```html 
<h1>Title 1</h1>
<h2>Title 2</h2>
<h3>Title 3</h3>
```

Visuellement:

<h1>Title 1</h1>
<h2>Title 2</h2>
<h3>Title 3</h3>


## 2 - Saute de lignes

Effectuer un saut de ligne simple dans votre texte markdown aura effet si vous terminez votre ligne par un double espace (ou plus que ça). Un retour chariot sera alors exporté ; c’est la balise `<br/>`.

Syntax markdown:

```html
Première ligne <br/>deuxième ligne <br/>troisième ligne
```

Visuellement:

Première ligne <br/>deuxième ligne <br/>troisième ligne

## 3 - L’emphase

Pour formater une partie de votre texte comme emphase, entourez le par des astérisques ou des underscores. Entourer par un signe unique passe en italique (emphase faible : *) et par un double signe en gras (emphase forte: **). 

Syntax markdown:

```md
*Une seule astérisque est italique*
_Une seule underscore est aussi italique_
**Deux plus astérisque surlignen plus le texte**
<b> Bold Letter </b>
```

Visuellement:

*Une seule astérisque est italique*

_Une seule underscore est aussi italique_

**Deux plus astérisque surlignen plus le texte**

<b> Bold Letter </b>

## 4 - Listes

*Option1.

Les listes non ordonnée. Commencez la ligne par une astérisque *, un moins - ou un plus +.

Syntax markdown:
```md
* item A
* item B
* item C
```

ou 

```md
- item A
- item B
- item C
```

ou 

```md
+ item A
+ item B
+ item C
```

Visuellement:


* item A
* item B
* item C

ou 

- item A
- item B
- item C

ou 

+ item A
+ item B
+ item C



*Option2. 

Pour afficher une liste ordonnée, commencez la ligne par un nombre suivit d’un point. 

Syntax markdown:

```md
1. item A
2. item B
3. item C
456. item D
```

Visuellement:

1. item A
2. item B
3. item C
456. item D


## 5. Tables

Il ya deux façon de faire les tableaux.

*Option 1. 

Avec pipe (|). L’idée globale est de “dessiner” des colonnes en les entourant avec des pipes |. Le nombre de colonnes est défini dans la première ligne du tableau et vous devez pour chaque ligne avoir le même nombre de colonnes, même si certaines sont vides.

Syntax markdown:

```md
| Name | Description | Age |        
|:--:|:--:|:--:|
|Mary| She is a nice girl | 20|
| Jackie Junior | He is a very smart boy | 5|
```


Visuellement:


| Name | Description | Age |       
|:--:|:--:|:--:|
|Mary| She is a nice girl | 20 |
| Jackie Junior | He is a very smart boy | 5|


*Option 2.

Avec l'aide du langage html. 

Syntax markdown:

```md
<table>
<tbody>
<thead>
<tr><th>Name</th><th>Description</th><th>Age</th></tr>
</thead>
<tr><td>Mary</td><td>She is a nice girl</td><td>20</td></tr>
<tr><td>Jackie Junior</td><td>He is a very smart boy</td><td>5</td></tr>
</tbody>
</table>
``` 

Visuellement:

<table>
<tbody>
<thead>
<tr><th>Name</th><th>Description</th><th>Age</th></tr>
</thead>
<tr><td>Mary</td><td>She is a nice girl</td><td>20</td></tr>
<tr><td>Jackie Junior</td><td>He is a very smart boy</td><td>5</td></tr>
</tbody>
</table>

## 6. Inserer liens

Il y a deux façons d’afficher un lien. De manière automatique en encadrant un lien par des chevrons. Il est alors cliquable et affiche l’url indiquée entre chevrons.

* Option 1.

Avec markdown.

Syntax markdown:

```md
[Link a Simplon] (https://simplon.co/)
```

Visuellement:

[Link à Simplon](https://simplon.co/)    <-- CLIQUEZ SUR LE LIEN!!!


* Option 2.

Avec html.

Syntax:
```html
<a href="https://simplon.co/" title="simplon.co">Link à Simplon</a>
```

Visuellement:

<a href="https://simplon.co/" title="simplon.co">Link à Simplon</a>   <-- CLIQUEZ SUR LE LIEN!!!


## 7. Inserer images

Il y a deux options, avec html ou pure markdown.

* Option1. 

Avec markdown.

Pour afficher une image, commencez par un point d’exclamation. Puis indiquez le texte alternatif entre crochets. Ce dernier sera affiché si l’image n’est pas chargé et lu par les moteurs de recherche. Terminez par l’URL de l’image entre parenthèses.

Syntax markdown:

```md
![Image de Simplon Grenoble](/static/simplon_gren.png)
```

Visuellement:

![Image de Simplon Grenoble](/wiki/static/simplon_gren.png)


* Option2. 

Avec html.

Syntax:

```html
<img src="/wiki/static/simplon_gren.png" alt="logo Simplon Grenoble" width='500px' height='300px'>
```

Visuellement:

<img src="/wiki/static/simplon_gren.png" alt="logo Simplon Grenoble" width='500px' height='300px'>


## 8. Des schémas graphiques

Mermaid vous permet de représenter des diagrammes à l'aide de texte et de code.

Syntax:

```
graph TD;
  A-->B;
  A-->C;
  B-->D;
  C-->D;
```


Visuellement:

```mermaid
graph TD;
  A-->B;
  A-->C;
  B-->D;
  C-->D;
```


## Références

[blog pour commencer à utiliser le markdown](https://blog.wax-o.com/2014/04/tutoriel-un-guide-pour-bien-commencer-avec-markdown/)

[Wiki markdown](https://fr.wikipedia.org/wiki/Markdown)

[markdown.org guide](https://www.markdownguide.org/getting-started/)

[mastering markdown]([https://guides.github.com/features/mastering-markdown/)

[mermaid](https://mermaid-js.github.io/mermaid/#/)

