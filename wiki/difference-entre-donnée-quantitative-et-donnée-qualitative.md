# Différence entre donnée quantitative et donnée qualitative

> On peut définir la "donnée" comme la présentation d'une information brute (ou pas) sur un ou plusieurs sujets, qui sert de base d'analyse ou d'étude de ce(s) sujet(s). Le type de données qui nous intéresse dans notre étude est la donnée de type statistique. La donnée statistique dégage deux sous types caractéristiques : donnée quantitative et donnée qualitative.

## Donnée Quantitative

La donnée quantitative peut être défini comme les informations concrètes, collectées sous forme numérique, qui peuvent être mesurées (poids, taille, etc.) ou repérées (ex : température)
. On peut l’utiliser notamment pour les analyses, les statistique, représentation graphique, etc. La notion de quantité ressort ici sous le caractère mesurable (ex : Le poids d’une personne) et le caractère repérable (ex : le point géométrique).

### Avantage des données quantitatifs :

 - Permet une vision d'ensemble sur le sujet que l'on traite
 - Il est facile de faire une analyse statistique avec ce type de données
 - Possibilité de représenter sous forme de graphique 
 - On peut ressortir la moyenne de ses données
 - Mesurable 

### Limites de ce type de données

 - Ce type de données néglige les renseignements personnels 
 - On ne peut les analyser pour d'écrire le pourquoi ou le comment des sujets de l'information

## Donnée Qualitative

Les données qualitatives sont des informations destinées à d'écrire un sujet plutôt qu'à le mesurer, un recueil d'avis ou d'opinions. Ici on ne peut attribuer une valeur mesurable à la donnée. Les notions de qualité du sujet et sa description sont les critères étudiés.  Contrairement à la donnée quantitative, ici les données sont **contextuelles**, on fait l’état des choses plutôt que de les mesurer (ex : les couleurs, l’état).

### Avantages des données qualitatives

 - Information contextuelle détaillée
 - Complète la donnée Quantitative
 - Description du sujet d'étude


### Limites des données qualitatives


 - Exige beaucoup plus de temps pour traiter
 - Parfois moins crédible que les données numériques
 - Impossible de faire la moyenne de ses données
 - Pas Mesurable


## Que retenir

La grande différence entre la données quantitative et la donnée qualitative réside dans le fait que la première est mesurable tandis que la deuxième est plutôt descriptive, contextuelle. Ceci ne veut pas dire qu'on ne peut avoir un chiffre comme donnée qualitative. Exemple de donnée qualitative en chiffre : Que chacun donne son chiffre préféré ?
 1. Paul = 7
 2. Jean = 1
 3. Jul = 4

 Ici on a des données chiffrées mais pas mesurable. On ne peut pas calculer la moyenne de ses chiffres car ils ne représentent pas une quantité mais plutôt un symbole. Il est donc important de faire attention au type de donnée dont on a à faire.
## Liens pour plus d'explications

https://fr.surveymonkey.com/mp/quantitative-vs-qualitative-research/

http://sgba-resource.ca/fr/?page_id=1342#:~:text=Donn%C3%A9es%20quantitatives%3A%20Information%20num%C3%A9rique%20ou,des%20photos

