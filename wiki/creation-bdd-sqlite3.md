# Comment créer une base de données **SQLite** ?


SQLite n'utilise pas l'instruction  **CREATE DATABASE** comme dans d'autres systèmes de gestion de bases de données, tels que MySQL, SQL Server, etc.


SQLite vous donne la possibilité de créer une nouvelle base de données (ou d'ouvrir une base de données existante) 

Donc, en d'autres termes, pour créer une nouvelle base de données en **SQLite**, il suffit d'entrer sqlite3 suivi du nom du fichier que vous souhaitez utiliser pour la base de données.

Le code suivant crée un fichier de base de données appelé **simplon**.db :
```
sqlite3 simplon.db ;
```
Les commandes **SQLite** se terminent par un point-virgule ( ; ). Cela indique à SQLite que votre commande est complète et doit être exécutée.

Si vous n'incluez pas le point-virgule, vous verrez une invite de suite, comme ceci

> ...>



ce qui signifie que SQLite attend que vous saisissiez d'autres choses. Il suffit d'ajouter le point-virgule et d'appuyer sur la touche Entrée pour exécuter la commande.




## Autres infos

Ce [**DOCUMENT**](http://zetcode.com/db/sqlite/tool/) contient des infos plus complète pour la pris en main de SQLite et ses commandes d'utilisation.

