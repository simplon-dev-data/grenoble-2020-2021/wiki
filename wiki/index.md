![logo](static/logo-simplon.svg "Logo Simplon")

# {{ config.site_description }}

Posez vos questions dans les ["issues"]({{ config.repo_url }}-/issues),
et centralisez les réponses automatiquement dans ce wiki !

Le wiki est autogénéré et déployé via GitLab Pages à chaque
["merge request"]({{ config.repo_url }}-/merge_requests)
dans la branche `master` !

Réalisé avec [MkDocs](https://www.mkdocs.org).
