# C'est quoi Gitlab ?

> Gitlab est une plateforme permettant d'héberger et de gérer des projets web. Gitlab est un outil essentiel pour tous développeurs experts 
comme amateurs. Se basant sur les fonctionnalités du logiciel [**GIT**](https://gitlab.com/simplon-dev-data/grenoble-2020-2021/wiki/-/blob/rayane-zein/wiki/wiki_what-is-git.md), 
elle permet de piloter des dépôts de code source et de gérer leurs différentes versions. 
Gitlab offre la fonctionalité d'un wiki pour une documentation sur les diverses projets ainsi qu'un systeme issues qui permet de poser des questions à la communauté.

## Exemple d'utilisation 

![branche](static/Gitlab_branch.png "Branche")

> Sur cet exemple, le master se lance dans un projet qui lui prend plusieurs heures, content de son résultat, sauvegarde
son projet dans sa première version 1.0. Durant son sommeil une idée de génie lui vient, il reprend sa version 1.0, rajoute son idée et sauvegarde dans une version 1.1. Apres avoir testé sa version 1.1 il remarque une incohérence, grace à Gitlab, sa version 1.0 est toujours existante. Il reprend donc cette version 1.0, fait à nouveau des modifications qui seront enregistré dans une version 1.3 et ainsi de suite.

> Avec la fonction issue, il demande de l'aide sur sa version 1.1. Un utilisateur décide de l'aider et reprend donc sa version 1.1, travaille dessus qui deviendra ensuite une version 2.0. Il decide de faire une dernière modification avant de le [**merge request**](https://gitlab.com/simplon-dev-data/grenoble-2020-2021/wiki/-/blob/ferrand-cinthya/wiki/template/page.md) au master sous une version 2.1. Le master satisfait de son aide decide d'implanter les modifications à sa branche sous une version 1.4.

> Un utilisateur 2 trouve ce projet interessant et decide de prendre la version 2.0 pour la base de son propre projet et y ajouter sa touche personelle, nous avons donc ici la version 3.0. Apres des heures de travail, son project est au complet dans sa version 3.2

## Références

- https://blog.axopen.com/2017/02/gitlab-cest-quoi/
- https://junto.fr/blog/gitlab/