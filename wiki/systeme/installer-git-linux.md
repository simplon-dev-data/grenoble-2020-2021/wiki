# Installer Git sous Linux

> Installer Git et configurer sa connexion SSH en quelques minutes

!!! tip "TL;DR"
    ```bash
    sudo apt install git ssh
    git config --global user.name <Prenom Nom>
    git config --global user.email <prenom.nom.simplon@gmail.com>
    ssh-keygen -t ed25519
    ssh -T git@gitlab.com
    ```

## Problème

J'ai créé mon compte sur [GitLab][gitlab], créé mon premier dépôt de code en ligne et je souhaite maintenant commencer à coder !
J'ai cru comprendre que j'ai besoin d'installer [Git][git] sur mon système Linux (Ubuntu). Aussi, on m'a conseillé d'utiliser le protocole [SSH][ssh], mais j'ai du mal à voir quel est le rapport.

N'ayons crainte, c'est beaucoup plus simple qu'il n'y parait !
Suivons le guide !

!!! note
    Cet article prend pour exemple GitLab mais la méthode
    reste sensiblement la même pour n'importe quelle autre plateforme
    ([GitHub][github], [BitBucket][bitbucket]).

## Solution

### Installation

Commençons par installer les logiciels nécessaires: [Git][git] et [SSH][ssh].

Dans une nouvelle fenêtre de "Terminal":

```bash
sudo apt install git ssh
```

### Configuration

Maintenant que les logiciels nécessaires sont installés, il faut les configurer.

#### Git

Git va nous permettre de versionner nos fichiers textes afin de ne
jamais perdre l'historique de nos modifications. Mais pour obtenir un
historique, il est nécessaire de savoir quel développeur a modifié quel
fichier à quel moment !

Introduisons-nous auprès de Git avec notre nom:

```bash
git config --global user.name <Prenom Nom>
```

et notre adresse e-mail:

```bash
git config --global user.email <prenom.nom.simplon@gmail.com>
```

Désormais, chaque _commit_ effectué sur votre machine sera rattaché
automatiquement à notre nom et notre adresse e-mail !

#### SSH

Le protocole SSH va permettre à Git de discuter avec la plateforme GitLab
en toute sécurité et simplicité : pas de mot de passe à taper à chaque
fois que l'on souhaite _pousser_ nos modifications, et une sécurité beaucoup plus forte que notre mot de passe ! Que demander de plus ?

!!! warning
    Il est possible d'utiliser Git **sans** SSH mais cela est fortement
    déconseillé, principalement pour les raisons évoquées ci-dessus.
    D'autre part, la majorité de l'industrie informatique utilisant
    désormais Git avec SSH, il est important de savoir le faire !

SSH utilise le principe d'une _identité numérique_: notre état civil est
public, mais nous seuls avons le secret de notre signature unique !

En informatique, le concept de [_cryptographie asymétrique_][cryptographie-asymetrique]
reprend ce même principe : notre identité est composée d'une
_clé publique_ permettant au monde extérieur de nous contacter, et d'une _clé privée_ nous permettant de prouver au monde extérieur qu'il s'agit bien de nous !

Il est temps de créer notre identité numérique !

Générons notre paire de clés SSH :

```bash
ssh-keygen -t ed25519
```

Il nous est demandé de protéger notre clé privée par un mot de passe, faisons-le :

```
Generating public/private ed25519 key pair.
Enter file in which to save the key (/home/user/.ssh/id_ed25519):
Enter passphrase (empty for no passphrase): <mon super mot de passe>
```

Notre nouvelle identité est stockée dans les fichiers suivants:

- `~/.ssh/id_ed25519`: notre clé secrète (comme son nom l'indique, elle ne doit pas quitter la machine)
- `~/.ssh/id_ed25519.pub`: notre clé publique (que nous allons pouvoir partager)

Nous devons maintenant informer GitLab de notre nouvelle identité :

1. Se connecter à son compte GitLab

2. Sélectionner l'avatar dans le coin haut droit et cliquer sur "Settings"

3. Cliquer sur "SSH Keys"

4. Copier le contenu de la clé publique

    ```bash
    cat ~/.ssh/id_ed25519.pub
    ```

5. Coller le contenu de la clé publique dans la section "Key"

6. Ajouter un nom explicite pour la clé dans "Title", tel que "Ubuntu Simplon"

7. Cliquer sur le bouton "Add key"

Nous venons de finir le plus dur : notre identité numérique est
en place sur notre machine et est associée à notre compte GitLab !

### Test

Testons que notre configuration Git + SSH fonctionne bien avec GitLab :

```bash
ssh -T git@gitlab.com
```

À la première connexion, nous devons accepter le message suivant :

```
The authenticity of host 'gitlab.com (35.231.145.151)' can't be established.
ECDSA key fingerprint is SHA256:HbW3g8zUjNSksFbqTiUWPWg2Bq1x8xdGUrliXFzSnUw.
Are you sure you want to continue connecting (yes/no)? yes
Warning: Permanently added 'gitlab.com' (ECDSA) to the list of known hosts.
```

Bravo ! Notre installation de Git avec SSH sous Linux est fonctionnelle !

## Références

- [Ubuntu and Git][ubuntu-git]
- [GitLab and SSH keys][gitlab-ssh]

[git]: https://git-scm.com
[ssh]: https://www.ssh.com/ssh/
[gitlab]: https://gitlab.com
[gitlab-ssh]: https://docs.gitlab.com/ce/ssh/
[github]: https://github.com
[bitbucket]: https://bitbucket.org
[ubuntu-git]: https://guide.ubuntu-fr.org/server/git.html
[cryptographie-asymetrique]: https://fr.wikipedia.org/wiki/Cryptographie_asym%C3%A9trique
