# Comment ajouter du contenu ?

> Ce wiki est une base de connaissance collaborative !

Que vous souhaitiez aider un autre apprenant en répondant à une
[question](poser-une-question.md) ou partager vos dernières
trouvailles au cours de la formation, ils est important de pouvoir
ajouter simplement du contenu.

La base de données du contenu étant stockée dans le dépôt GitLab du projet,
nous allons utiliser le principe des "Merge Requests" offert par la plateforme.

!!! tip
    L'ajout de contenu peut se faire via l'interface web de GitLab et
    en local sur votre machine.

## Ecrire une page

Les pages du wiki sont écrites en langage [Markdown][markdown].
Pas d'inquiétude, ce langage très simple est fait pour structurer
simplement un fichier texte de façon lisible par un humain !

Vous pouvez déjà explorer les pages existantes pour vous inspirer de la syntaxe : ouvrez un fichier `.md` dans le dossier [wiki][wiki-contenu].

Des _templates_ sont à votre disposition afin de partir d'une page existante :

- [Template page](template/page.md)
- [Template Jupyter Notebook](template/notebook.ipynb)

Placer votre nouvelle page dans le sous-dossier correspondant à la bonne
catégorie. Si la catégorie n'existe pas encore, créer simplement le
sous-dossier dans le dossier [wiki][wiki-contenu].

[Créer une page !][creer-page]

!!! warning
    Attention au nommage des fichiers et dossiers :

    - Nommer les pages **uniquement en minuscules, sans accents, sans espaces, avec des tirets séparateurs, et sans prépositions !** Par exemple le fichier d'une page nommée "Le méga guide du développeur !" doit se nommer `mega-guide-developpeur.md`
    - Nommer les dossiers de catégories **uniquement d'un seul mot en minuscules sans accents" !** Par exemple, un dossier d'une catégorie "Développement" doit se nommer `developpement`

## Sauvegarder une page

Pour sauvegarder une page, nous allons utiliser la fonction principale de
Git : les branches !

Créer une branche à partir de `master` :

```bash
git checkout -b ajout-page-xxx master
```

Ajouter la page à commiter :

```bash
git add wiki/<categorie>/page-xxx.md
```

Commiter la page :

```bash
git commit -m "Ajout page xxx"
```

Pousser la branche :

```bash
git push origin ajout-page-xxx
```

## Soumettre la page

Une fois la nouvelle branche poussée sur le projet GitLab, il suffit de cliquer sur le bouton "Create merge request" en haut à droite :

![branche](static/ajout-branche.png "Branche")

Pour créer la Merge Request :

1. Sélectionner le template "template_contenu"
2. Remplir le texte de la Merge Request en suivant le modèle
3. Sélectionner le "Label" (catégorie)
4. Cliquer sur "Submit merge request"

![mr-creation](static/ajout-mr-creation.png "Merge Request Creation")

Vérifier que la Merge Request est assignée à un collaborateur
pouvant effectuer la validation :

![mr-statut](static/ajout-mr-statut.png "Merge Request Statut")

Il ne reste plus qu'à attendre l'approbation et/ou les éventuelles
modifications à effectuer avant fusion dans la branche `master`.

Une fois la Merge Request acceptée et fusionnée, votre page apparaitra
automatiquement après quelques minutes dans la table des matières du
wiki en ligne !

!!! tip
    En attendant la validation de la Merge Request, vous pouvez observer
    le statut du _pipeline d'intégration continue_ : un robot exécute une
    série d'instructions afin de valider que vos changements s'intègrent
    correctement avec le reste de l'application !

[markdown]: https://www.markdownguide.org
[wiki-contenu]: {{ config.repo_url }}-/tree/master/wiki
[creer-page]: {{ config.repo_url }}-/new/master/wiki
