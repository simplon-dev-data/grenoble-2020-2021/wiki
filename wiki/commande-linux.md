# Commandes linux

## Les commandes Unix de base à connaître par cœur :

- ls: 
Permet de lister un répertoire.

`[root@centos home]# ls
user1 user2`

On trouve d'autres options comme:
- [ ] ls -l : Permet un affichage détaillé du répertoire (permissions d'accès, le nombre de liens physiques, le nom du propriétaire et du groupe, la taille en octets, et l'horodatage)

- [ ] ls -h : Associé avec -l affiche la taille des fichiers avec un suffixe correspondant à l'unité (K, M, G)
- [ ] ls -a : Permet l'affichage des fichiers et répertoires cachés.


- cd: 
Permet de se promener dans les répertoires
 
- [ ] cd : permet de revenir au répertoire /home/utilisateur

- [ ] cd - : permet de revenir au répertoire précédent

- [ ] cd .. : permet de remonter au répertoire parent

- [ ] cd / : permet de remonter à la racine de l'ensemble du système de fichiers

- cp: 
Permet de copier des fichiers ou des répertoires, les options les plus fréquentes sont les suivantes :

- [ ] -a : archive, copie en gardant les droits, dates, propriétaires, groupes, etc.
 
- [ ] -i : demande une confirmation avant d'écraser

- [ ] -f : si le fichier de destination existe et ne peut être ouvert alors le détruire et essayer à nouveau

- [ ] -r : copie un répertoire et tout son contenu, y compris les éventuels sous-répertoires

- [ ] -u : ne copie que les fichiers plus récents ou qui n'existent pas
- [ ] -v : permet de suivre les copies réalisées en temps réel

- rm: 
Permet d'effacer des fichiers, cette commande est très dangéreuse, cette commande est à utiliser avec précaution car avec l’option -f ou -rf, cette commande peut endommager voir supprimer tout votre système de fichiers de manière irréversible.

 les options les plus fréquentes :

 - [ ] -i : demande confirmation avant d'effacer
 - [ ] -f : ne demande pas de confirmation avant d'effacer
 - [ ] -r : efface récursivement

- mkdir: 
Permet de crée un répertoire vide, il existe aussi l'option :
- [ ] -p : crée les répertoires parents s'ils n'existent pas

`[root@centos tmp]# mkdir repertoiredetest`
`[root@centos tmp]# ls
repertoiredetest
fichiertemp`

- rmdir: 
Supprime un répertoire vide, comme option on trouve:
- [ ] -p : supprime les répertoires parents s'ils deviennent vides


- pwd: 
Cette commande permet d'afficher le répertoire de travail


- cat: 
Permet d'afficher le contenu d'un fichier, parmis les options les plus fréquentes on trouve : 

- [ ] -n : affiche les numéros de ligne
- [ ] -v : affiche les caractères de contrôles

- touch:
Cette commande permet de changer la date du dernier accès ou modification d’un fichier, mais permet également de créer un fichier vide.

`[root@centos tmp]# touch fichier`

`[root@centos tmp]# ls
fichier`

- echo: 
Cette commande permet d’afficher une ligne.

`[root@centos tmp]# echo "tutoriel Linux de sitedetout"
tutoriel Linux de sitedetout`

Elle permet aussi d’écrire du contenu dans un fichier moyennant le signe “>” pour écraser le contenu du fichier ou “>>” pour suffixer le contenu du fichier.

- head: 
Permet d’afficher le début d’un fichier (par défaut, les 10 premières lignes)





## Commandes système
On trouve aussi les commandes système:

- chmod: 
Modifie les permissions d'accès à un fichier ou à un répertoire.

Type d'autorisations par exemple :

- [ ] + : Ajoute une permission
- [ ] - : Enlève une permission
- [ ] x : Exécution ; Valeur octale 1

Parmis les options les plus fréquentes on trouvera :


- [ ] -R : récursif, modifie les autorisations d'un répertoire et tout ce qu'il contient
- [ ] -c : ne montrer que les fichiers ayant été réellement modifiés
- [ ] -f : ne pas afficher les messages d'erreur


- sudo:
Permet d'exécuter des commandes en tant qu'un autre utilisateur, donc avec d'autres privilèges que les siens.
Options les plus fréquentes :

- [ ] -s : importe les variables d'environnement du shell
- [ ] -k : lorsque l'on utilise sudo, il garde en mémoire le mot de passe ; cette option déconnecte l'utilisateur et forcera à redemander un mot de passe si sudo est exécuté avant le timeout défini.


- apt-get:
Permet l'​installation et la désinstallation ​de paquets ​en tenant compte des dépendances ainsi que le   téléchargement des paquets ​s'ils sont sur une source réseau.

Parmis les Commandes les plus utiliseés :


- [ ] update : met à jour la liste des paquets disponibles en fonction des sources fournies.

- [ ] install : installe un ou plusieurs paquets.

- [ ] remove : supprime un ou plusieurs paquets.
- [ ] clean : efface du système ​les installateurs,​ sans désinstaller de paquets.

 Options les plus fréquentes :

- [ ] -f : utilisée avec install ou remove cette option permet de réparer un système dont les dépendances sont défectueuses.

- [ ] -m : ignore les paquets manquants (à éviter si on ne sait pas exactement ce que l'on fait).
- [ ] -u : affiche les paquets mis à jour.


IL existe encore d'autres commandes Linux, vous trouvez plus de détail sur leurs fonctionnements et leurs différents options dans les liens suivant .

https://doc.ubuntu-fr.org/tutoriel/console_commandes_de_base#apt-get

https://www.sitedetout.com/tutoriels/commandes-linux-de-base/





