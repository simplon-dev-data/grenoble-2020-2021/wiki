# Quelles sont les commandes essentielles du shell?

Voici un aperçu des possibilités qu'offre le shell Linux, avec une liste non exhaustive des commandes qui vous seront utiles!

# TLDR

L'article est présenté sous la forme d'un court tutoriel, où vous pourrez exécuter vos premières commandes pas à pas. Si la longueur de cet article vous rebutte, rendez-vous à cette adresse:  
https://m.youtube.com/watch?v=bb_ApuH15YE  
Cette petit vidéo (en anglais) vous explique tout en 5 minutes!

# C'est parti!

## Qu'est-ce qu'un shell?

Un shell Unix est un interpréteur de commandes destiné aux systèmes d'exploitation Unix qui permet d'accéder aux fonctionnalités internes du système d'exploitation. 
Aujourd'hui, bash est le shell le plus répandu, bien qu'il existe d'autres interpréteurs de commandes. Il est l'interprète par défaut sur de nombreux Unix libres, notamment sur les systèmes GNU/Linux.

## Comment utiliser le shell?

Le shell se présente sous la forme d'une interface en ligne de commande accessible depuis le terminal. L'utilisateur lance des commandes sous forme d'une entrée texte exécutée ensuite par le shell.
Pour chaque action que vous voulez exécuter via le terminal, utilisez un appel de programme selon le schéma de base suivant :
```bash
COMMANDE [OPTIONS] [ARGUMENTS]
```
Remarques:  
Les options permettent d'accéder à des fonctions supplémentaires des programmes:  
Les options courtes, qui s'écrivent en une seule lettre, sont appelées par un tiret simple.  
Les options longues, qui s'écrivent en un mot, sont appelées par deux tirets.  
Les programmes ou les options nécessitent parfois des arguments qu'il faudra spécifier, sous peine de recevoir un message d'erreur. 

## Premiers pas dans le shell

Dans l'invite de commande, taper "help"
```
toto@Toto:~$ help
```
La commande s'execute et apparait une liste de commande, non exhaustive, de commandes du shell.

Pour avoir plus de détails sur une commande, on fait appel à "man". Dans l'invite de commandes, taper man suivi de la commande de votre choix, par exemple:
```
toto@Toto:~$ man echo 
```
La commande renvoie le détail des options et arguments applicables à la commande "echo". On apprend la fonction de la commande "echo":
```
NAME
       echo - display a line of text
```
Dans la suite de cet article, nous obtiendrons une description des commandes en utilisant le schéma suivant:
```
COMMANDE --help
```
Ce schéma fonctionne pour la plupart des commandes. Sans rentrer dans le détail, les exceptions à ce schéma sont liées à la conception du shell et du système qui diffèrent selon les distributions.  

CE QU'IL FAUT RETENIR!
```
COMMANDE --help
```
fournit une explication suffisante dans la plupart des cas, pour des explications plus détaillées ou dans les cas ou le schéma ci-dessus ne fonctionnerait pas, on utilise le schéma suivant:
```
man COMMANDE
```

## Quelles commandes utilisées en fonction de mes besoins? 


### Pour gérer des répertoires:

#### Savoir quel est le dossier courant avec "pwd"

```
toto@Toto:~$ pwd --help
pwd: pwd [-LP]
   Affiche le nom du répertoire de travail courant.
   ...  
```
exemple:
```
toto@Toto:~$ pwd
/home/toto
```
#### Lister les dossiers et les fichiers avec "ls"
```
toto@Toto:~$ ls --help
Utilisation : ls [OPTION]... [FICHIER]...
Afficher des renseignements sur les FICHIERs (du répertoire actuel par défaut).
Trier les entrées alphabétiquement si aucune des options -cftuvSUX ou --sort
ne sont utilisées.
...
```
exemple:
```
toto@Toto:~$ ls
 activites             from_gitlab                prairie
 Bureau                groupe-scolaire-2020.csv   Public
'CommandesLinux&Git'   Images                     python-decouverte
 conteneurs.db         mbox                       RecherchePython
 Documents             methodologie.md            snap
 DocuPourLeWiki        Modèles                    Téléchargements
 Edge_computing        Musique                    Vidéos
```

#### Changer de répertoire courant avec la commande "cd"

```
toto@Toto:~$ cd --help
cd: cd [-L|[-P [-e]] [-@]] [rép]
    Change le répertoire de travail du shell.  
```
exemple:
```
toto@Toto:~$ cd Musique
toto@Toto:~/Musique$
toto@Toto:~/Musique$ pwd
/home/toto/Musique
toto@Toto:~/Musique$ 
```
Le répertoire courant a changé.

#### Créer ou supprimer un dossier avec les commandes "mkdir" et "rmdir"

```
toto@Toto:~/Musique$ mkdir --help
Utilisation : mkdir [OPTION]... RÉPERTOIRE... 
Créer le ou les RÉPERTOIREs s'ils n'existent pas.
...
```
Créons un nouveau dossier "Rolling_Stones" dans le répertoire courant:
```
toto@Toto:~/Musique$ mkdir Rolling_Stones
toto@Toto:~/Musique$ ls
Rolling_Stones
toto@Toto:~/Musique$ 
```
Le dossier "Rolling_Stones" a été crée dans le répertoire courant.
```
toto@Toto:~/Musique$ rmdir --help
Utilisation : rmdir [OPTION]... RÉPERTOIRE...
Supprimer le ou les RÉPERTOIRE, s'ils sont vides.
...
```
Regardons comment utiliser la commande "rmdir":
```
toto@Toto:~/Musique$ rmdir --help
Utilisation : rmdir [OPTION]... RÉPERTOIRE...
Supprimer le ou les RÉPERTOIRE, s'ils sont vides.
...
```
Supprimons le dossier "Rolling_Stones" avec "rmdir":
```
toto@Toto:~/Musique$ rmdir Rolling_Stones
gabriel@Gabriel:~/Musique$ ls
gabriel@Gabriel:~/Musique
```
Le fichier n'apparaît plus lors de l'appel de la commande "ls"

Remarque:
Comme spécifié dans l'aide de "rmdir", le répertoire doit être vide. Il faut vider au préalable le répertoire pour l'utiliser. Nous verrons dans la partie "Manipuler des fichiers" comment supprimer les fichiers contenus dans le répertoire.  

### Manipuler des fichiers:

#### Créer un fichier vide avec la commande "touch"
```
toto@Toto:~$ touch --help
Utilisation : touch [OPTION]... FICHIER...
Mettre à jour la date d`accès et de modification de FICHIER à l`heure actuelle.

Un argument FICHIER n'existant pas est créé vide sauf si -c ou -h sont indiqués.
...
```
La commande "touch", initialement prévue pour mettre à jour les informations de fichier, crée automatiquement le fichier s'il n'existe pas. Nous allons l'utiliser pour créer un nouveau fichier texte.
```
toto@Toto:~$ touch "ceci_est_un_texte.md"
```
#### Editer un fichier texte à la volée avec la commande "nano"
```
toto@Toto:~$ nano --help
Utilisation : nano [OPTIONS] [[+LIGNE[,COLONNE]] FICHIER]...
...
```
```
toto@Toto:~$ nano "ceci_est_un_texte.md"
```
La commande ouvre le fichier dans l'éditeur de texte "nano", on peut alors insérer du texte.

#### Afficher le contenu d'un fichier texte à la volée avec la commande "cat"
```
toto@Toto:~$ cat --help
Utilisation : cat [OPTION]... [FICHIER]...
Concaténer les FICHIERs vers la sortie standard.
...
```
```
toto@Toto:~$ cat ceci_est_un_texte.md 
blabla
```
Le texte édité avec la commande "nano" s'affiche.  

#### Supprimer un fichier avec la commande "rm"
```
toto@Toto:~$ rm --help
Utilisation : rm [OPTION]... [FICHIER]...
Supprimer (retirer le lien) le ou les FICHIERs.
```
Nous allons supprimer le fichier que nous avons créé:
```
toto@Toto:~$ rm --help
Utilisation : rm [OPTION]... [FICHIER]...
Supprimer (retirer le lien) le ou les FICHIERs.
```
```
toto@Toto:~$ rm ceci_est_un_texte.md 
```
```
toto@Toto:~$ ls
 activites             Edge_computing             Modèles             snap
 Bureau                from_gitlab                Musique             Téléchargements
'CommandesLinux&Git'   groupe-scolaire-2020.csv   prairie             Vidéos
 conteneurs.db         Images                     Public
 Documents             mbox                       python-decouverte
 DocuPourLeWiki        methodologie.md            RecherchePython


```

### Nettoyer le terminal et quitter

Maintenant que vous maitrisez les commandes essentielles du shell, vous pouvez éventuellement nettoyer le shell des commandes que vous avez entrées et quitter le shell.
```
toto@Toto:~$man clear
...
NAME
       clear - clear the terminal screen
```
```
toto@Toto:~$clear
```
L'historique s'est effacé!
Il ne vous reste plus qu'à quitter le shell:
```
toto@Toto:~$quit
```
Le terminal disparaît!

# Références
https://m.youtube.com/watch?v=bb_ApuH15YE  
https://www.gnu.org/software/coreutils/  
https://fr.wikipedia.org/wiki/Shell_Unix  
https://fr.wikipedia.org/wiki/Bourne-Again_shell  
https://www.ionos.fr/digitalguide/serveur/configuration/commandes-linux/
