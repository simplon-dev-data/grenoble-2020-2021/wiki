# Qu'est ce que Git ?
 
Que vous soyez féru de bidouillage en tout genre, adepte de customisation ou hacker en herbe, il vous est sûrement déjà arrivé de vous perdre sur des forums vous renvoyant à des sites web tels que Github, Gitlab, GitBucket ces sites au nom “Giteux” où des communautés actives dans l’univers du développement vous propose des lignes de code, pour vous aider dans votre conquête du monde ont en commun “ Git “. Ne cherchez pas de lien entre le mot Git et votre mégalomanie, ce wiki vous propose une brève description de ce qu’est “Git”.
 
## I am Git, nice to meet you !
 
Git est le nom donné par Linus Torvalds, l'inventeur du noyau Linux lui-même, à son logiciel de gestion de version.
La première version de Git sortie le 07 avril 2005 se veut de crier une architecture épurée, avec un petit nombre de structures de données élémentaires, simple et performant dont la principale fonction est de gérer l'évolution de contenus dans une arborescence.
## Problème
 
Au fur et à mesure des projets de développement, les auteurs et contributeurs créent, modifient, et font évoluer les codes destinés à faire fonctionner les logiciels. Le nombre parfois important de contributeurs mais aussi de modifications qui interviennent dans le cycle de développement d'un programme informatique, rendent difficile le versionnage des différentes composantes qui constituent le logiciel dans sa version finale. Se pose alors le problème du versionnage, de l'historisation, et de la possibilité de revenir en arrière en cas de problème.
 
## Solution
 
### Indexation par somme de contrôle
 
Avec Git, le stockage des fichiers contenant le code source d'un programme informatique se fait selon une arborescence. Chaque nouveau fichier ajouté à la racine de cet arborescence ou dans l'un de ses sous-répertoire est identifié par Git grâce à sa "somme de contrôle".
 
### La somme de contrôle
 
La somme de contrôle est un nombre au format hexadécimal qui est calculé à partir du contenu d'un fichier (succession de chaînes de caractères ASCII). Ainsi un fichier texte contenant la chaîne de caractère "Git", n'aura pas la même somme de contrôle que ce même fichier dupliquer et modifier où on aurait remplacé le "G" par un "B", donnant une chaîne de caractère "Bit", le "G" et le "B" ayant une valeur hexadécimal différente, respectivement 47 et 42, leurs somme de contrôle est différente.
 
Ainsi, pour éviter de stocker des doublons, et répondre au problème du versionage, Git compare les sommes de contrôle des fichiers et ne les stocke que s'ils sont différents. Il permet alors de construire des archives d'un fichier dont le code source aurait changé au fur et à mesure de son développement afin de le restaurer en cas de besoin.
 
### La décentralisation
 
Pour favoriser la collaboration entre les contributeurs d'un projet, Git à la particularité d'être un système "décentralisé". Cela veut dire qu'il offre la possibilité à chaque contributeur,d'importer une arborescence et son contenu sur son ordinateur personnel. Ainsi, il n'est pas obligatoire de devoir se connecter à un serveur central pour accéder au projet.
 
Cette cartographie se fait selon le protocole peer-to-peer. Ce qui signifie que l'ordinateur de chaque contributeur peut être à la fois un serveur, qui distribue une arborescence, et un client qui reçoit une arborescence via les commandes que propose Git.
 
### Les commandes principales de Git
 
Git stocke le contenu suivant une arborescence. Le dossier racine d'une arborescence est un appelé "repository", qui signifie dépôt en français. Dans un nouveau dépôt il est possible de créer des sous-dossier pour y ranger des fichiers.
 
Les migrations de contenu entre un serveur et un client Git se fait grâce aux commandes que propose Git :
 
`git init` permet de créer un nouveau dépôt Git\
`git clone https:///url-du-depot-distant.com` : permet d'importer un dépôt depuis un serveur distant.\
`git add` : permet d'ajouter les modifications à déployer dans l'arborescence d'un dépôt.\
`git commit` : permet de déployer les modifications sur l'arborescence modifiée.\
`git branch` : permet d'afficher l'ensemble des branches dupliquée depuis l'arborescence mère.\
`git merge` : permet de fusionner une branche dans une autre.\
`git push` : permet de transférer les dernières modifications d'une arborescence locale vers le dépôt distant.\
`git pull` : permet de récupérer en local les dernières modifications d'une arborescence distante.
 
 

