# Comment atomiser une colonne ?

## Les colonnes à valeurs multiples

Lorsqu'on a une table avec une colonne contenant plusieurs valeurs, comme dans l'exemple suivant :

|choses|langages|
|-|-|
|truc|SQL, Python, R|
|bidule|SQL|
|machin|Python, R|

On peut garder la colonne telle quelle, mais ce n'est pas idéal.  
En effet dans ce cas on sera obligé de traiter la colonne pour l'utiliser.  
De plus, ce format va limiter le choix du type de donnée(s), qui sera souvent (toujours ?) obligatoirement du texte.  
Par exemple, si on souhaite connaître le nombre de languages associés à la valeur `truc`, on est alors obligé de traiter la chaîne de caractère en séparant les différentes valeurs et en retirant les virgules.

## Les façons non-optimales de traiter le problème

Pour retirer ces problèmes, on pourrait transformer la colonne d'une des deux manières suivantes, qui ne seront cependant pas idéales non plus :

> cas 1 :  
> Scinder la colonne en plusieurs. On est alors obligé de recréer une colonne à chaque fois qu'un nouveau language apparaît dans notre jeu de données.

|choses|sql|python|r|
|-|-|-|-|
|truc|true|true|true|
|bidule|true|false|false|
|machin|false|true|true|

> cas 2 :  
> Scinder les lignes contenant plusieurs valeurs en plusieurs. On est alors obligé de dupliquer des lignes et de perdre du sens pour la table (i.e.: `truc` est unique et contient plusieurs langages, mais dans cet exemple `truc` apparaît sur plusieurs lignes.)  
> De plus, on note la redondance des valeurs de la table. (qu'il vaut mieux éviter)

|choses|langages|
|-|-|
|truc|SQL|
|truc|Python|
|truc|R|
|bidule|SQL|
|machin|Python|
|machin|R|

## La façon optimale de traiter le problème

Sinon, on peut utiliser une table annexe pour lier les valeurs 

|id|choses|
|-|-|
|0|truc|
|1|bidule|
|2|machin|

|choses_id|langages|
|-|-|
|0|SQL|
|0|Python|
|0|R|
|1|SQL|
|2|Python|
|2|R|

De cette façon, la table principale est épurée et aucune cellule ne contient plusieurs valeurs.

# La table d'association

On note que dans l'exemple précédent, la redondance est encore présente pour les langages, qui sont répétés à travers les lignes de la table.  
Il est donc préférable de construire une table de référence pour les lister.  
On obtient donc :

|id|choses|
|-|-|
|0|truc|
|1|bidule|
|2|machin|

|id|langages|
|-|-|
|0|SQL|
|1|Python|
|2|R|


vvvvvvvvvvvv **Table d'association** vvvvvvvvvvvv

|choses_id|langages_id|
|-|-|
|0|0|
|0|1|
|0|2|
|1|0|
|2|1|
|2|2|

^^^^^^^^^^^^ **Table d'association** ^^^^^^^^^^^^

Cette troisième table s'appelle une table d'association. C'est l'équivalent des cellules d'une table pivot dans un tableur par exemple. (les deux autres tables vaudraient donc les lignes et les colonnes)  
Grâce à cette table enregistrant tous les liens entre les valeurs, on obtient deux tables plus petites et plus claires.  
On a également supprimé la redondance de la colonne `langage` sans en créer une pour la colonne `choses`.

&nbsp;  
&nbsp;