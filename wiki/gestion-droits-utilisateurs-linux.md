# Gestion des droits d'utilisateur sur les dossiers et fichiers dans un environnement Linux

Cet article présente simplement la lecture, et l'attribution des droits utilisateurs sous Linux.

## TLDR

Linux est un système multi-utilisateurs. Cela signifie que plusieurs personnes peuvent travailler simultanément sur le même OS. Puisque plusieurs utilisateurs peuvent être connectés à Linux en même temps, celui-ci doit avoir une très bonne organisation dès le départ. Chaque personne a donc son propre compte utilisateur, et il existe un ensemble de règles qui disent qui a le droit de faire quoi.


## La commande sudo

Nous allons commencer par une commande très importante dans la gestion des droits sous Linux, la commande sudo.

Pars défaut Linux nous crée lors de son installation un compte utilisateur limité dans les actions qu'il peut réaliser. Cela s'explique par le fait que certaines commandes peuvent être dangereuses pour la stabilité et la sécurité de notre ordinateur.

La commande sudo nous donne tous les droits possibles (mode root) sur une seule commande du terminal.

sudo = devenir root un instant

```bash
sudo apt-get install mon_programme
```

Par exemple avec cette commande, je peux installer sur ma machine le programme que je souhaite en mode root. Sans le sudo je ne pourrais pas le faire. 

## Organisation des utilisateurs sous linux

Linux gère les utilisateurs d'une certaine façon. En effet, il permet de créer autant d'utilisateur que l'on veut et de les organiser au seins de groupe.

Sur nos machines par exemple, il existe normalement pour l'instant que 2 utilisateurs, notre compte utilisateur limité et le superutilisateur root qui a tous les droit et qui appartient au groupe du même nom, root.

La notion d'utilisateur et de groupe est importante car Linux s'appuie sur celle-ci pour gérer les droits sur les fichiers et documents. 

Voici l'organisation plus détaillé : 

- Utilisateur : Utilisateur connecté au système. La liste des utilisateurs est disponible dans le fichier etc/passwd.
- Groupe : Groupe appartenant au système. La liste des groupes est disponible dans le fichier /etc/group

- Utilisateur Propriétaire (noté u comme user) : Utilisateur qui est en possession du fichier

- Groupe Propriétaire (noté g comme group): Groupe d'utilisateurs qui est en possession du fichier

- Autres Utilisateurs (noté o comme other): Utilisateurs qui ne sont ni propriétaire du fichier, ni faisant partie du groupe propriétaire.

- Tous (noté a comme all): Utilisateur propriétaire + Groupe propriétaire + Autres utilisateurs.


## Les permissions Linux

Plusieurs permissions peuvent être appliquées sur les fichiers/dossiers :

- r (pour read) : droit de lire le fichier/dossier
- w (pour write) : droit de modifier le fichier/dossier
- x (pour execute) : droit d’exécuter le fichier
- '-' (pour rien) : aucun droit sur le fichier/dossier

On peux acceder a ces information grace à la commande 

```bash
ls -l
```

Exemple de résultat : 

```bash
drwxr-xr-x 2 merouane merouane    4096 nov.  23 16:19 Modèles
drwxr-xr-x 2 merouane merouane    4096 nov.  23 16:19 Musique
drwxrwxr-x 2 merouane merouane    4096 déc.   9 09:31 projet_perso
drwxr-xr-x 2 merouane merouane    4096 nov.  23 16:19 Public
drwxrwxr-x 4 merouane merouane    4096 déc.   8 15:28 python-decouverte
drwxr-xr-x 6 merouane merouane    4096 déc.   8 11:45 snap
drwxr-xr-x 2 merouane merouane    4096 déc.   8 17:36 Téléchargements
drwxr-xr-x 2 merouane merouane    4096 nov.  23 16:19 Vidéos
```
On retrouve bien nos permissions r w x -.

La première ligne nous donne le type ici d pour directory ensuite nos permissions sont organiser par groupes de 3, les 3 premières corresponde a l'utilisateur ici merouane les 3 secondes au groupe ici toujours merouane et enfin les 3 dernière corresponde a tout le monde. 

Par exemple, le fichier Musique permet à l'utilisateur merouane la lecteur (r), l'écriture (w) et l'exécution (x). Alors que pour le groupe et pour le reste des utilisateur linux accorde seulement la lectur et l'exécution (r-x).

 schéma explicatif : 
 ```bash
 rwxr-xr--
 \ /\ /\ /
  v  v  v
  |  |  droits des autres utilisateurs (o)
  |  |
  |  droits des utilisateurs appartenant au groupe (g)
  |
 droits du propriétaire (u)
```

## Gestion des permissions

Toutes les permissions vues précédemment peuvent être changées grâce a une commande Linux, la commande chmod.

Fonctionnement de la commande chmod : 

Cette commande prend en premier argument 3 chiffres allant de 0 à 7.

Pour comprendre, chaque permission possède un chiffre :

- r (read) = 4
- w (write) = 2
- x (execute) = 1
- '-' = 0

En additionnant ces chiffres nous pouvons obtenir les permissions souhaitées.

Par exemple:

```bash 
chmod 777 mon_fichier
```
Cette commande donne tous les droits (lecture, écriture, exécution) à l'utilisateur, au groupe et à tous les autres utilisateurs.

utilisateur = 4(r)+ 2(w) + 1(x) = 7

groupe = 4(r)+ 2(w) + 1(x) = 7

tous = 4(r)+ 2(w) + 1(x) = 7

```bash 
chmod 755 mon_fichier
```
Cette commande donne tous les droits (lecture, écriture, exécution (rwx)) à l'utilisateur, mais le groupe et les autres utilisateurs ne pourront que le lire et l'exécuter sans pouvoir le modifier (r-x)

utilisateur = 4(r)+ 2(w) + 1(x) = 7

groupe = 4(r)+ 0(w) + 1(x) = 5

Tous = 4(r)+ 0(w) + 1(x) = 5

```bash 
chmod 000 mon_fichier
```
Cette commande enlève tous les droits à tous le monde.

## Conclusion

Nous avons donc vu ici la gestion des droits d'accès sous linux grâce aux utilisateurs et aux groupes. De plus, nous avons vu comment voir et modifier les droits pour chaque catégorie (utilisateur propriétaire, groupe et tout le monde) avec la commande chmod.


## Références

https://openclassrooms.com/fr/courses/43538-reprenez-le-controle-a-laide-de-linux/39044-les-utilisateurs-et-les-droits

https://www.linuxtricks.fr/wiki/droits-sous-linux-utilisateurs-groupes-permissions
