# Qu'est-ce que le 'path', ou "chemin d'accès"?

*"Lexique & Infos-clefs" accessible en fin d'article.*

## A quoi sert le path ?

L'ordinateur en a besoin pour savoir où sont rangés les fichiers.   
Il est toujours nécessaire de spécifier le path pour pouvoir exécuter une commande, créer un dossier, etc.  
En fait, lorsqu'on ne spécifie pas le path et que ça marche, c'est qu'une fonction cachée permet de sélectionner automatiquement son path actuel.

## Comment s'écrit un path ?

Le path qui suit (dans le graph) s'écrit comme ceci :  
`
/home/<username>/prairie/data-pipeline/README.md
`  
(En local sur Linux, on part de 'Computer', qui ne s'écrit pas)  

On peut donc, par exemple, ouvrir le fichier "README.md" dans l'éditeur de texte "nano" du dossier "data-pipeline" de la "prairie" depuis n'importe quelle position dans les dossiers, avec la commande suivante :  
`
nano /home/<username>/prairie/data-pipeline/README.md
`  
>&#x26A0; les dossiers ***ne*** sont ***pas*** *obligatoirement* les mêmes pour tout le monde.  
par exemple : 'prairie' peut être situé ailleurs : cela dépend d'où on l'a mis.  

>&#x26A0; à ne pas oublier le premier `/`, le "dossier d'origine", c'est l'ordinateur (Computer), même si on ne l'écrit pas.  

```mermaid
graph TD;
Computer-- "Includes" -->home;
Computer-- "Includes" -->etc[etc];
home-- "Includes" -->username;
username-- "Includes" -->prairie;
username-- "Includes" -->Documents;
username-- "Includes" -->Videos;
username-- "Includes" -->etc2[etc];
prairie-- "Includes" -->data-pipeline;
prairie-- "Includes" -->git_commands;
prairie-- "Includes" -->google-sheets;
prairie-- "Includes" -->etc3[etc];
data-pipeline-- "Includes" -->README.md;
data-pipeline-- "Includes" -->etc4[etc];
style data-pipeline fill:#A0ffA0
style README.md fill:#50B050
style Computer fill:#A0A0A0
style home fill:#A0ffA0
style username fill:#A0ffA0
style prairie fill:#A0ffA0
```
## Et dans le terminal ?

`pwd` est une commande qui montre son path actuel complet.  
Le path est également affiché en permanence après le ":" et avant le "$" qui est devant l'endroit où on écrit.

>&#x26A0; Dans le terminal, on se trouve toujours à un point précis du path, autrement dit dans un dossier. Nos commandes s'exécutent depuis cet endroit dans les dossiers.  

\
\
![Erreur dans le lien vers l'image : path_terminal_001](./images/path_terminal_001.png "terminal_path")

## Et dans GitLab ?

Le path commencera par le nom du projet, et suivra les dossiers de la même manière.

## Lexique & Infos-clefs

Le 'terminal' permet d'utiliser le 'shell' en language 'bash', par défaut.  
`Terminal` = le logiciel qu'on peut utiliser pour donner des commandes à l'ordinateur grâce au 'shell'.  
`Shell` = par défaut, le terminal nous fait utiliser le 'shell'.  
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Par exemple, on quitte le 'shell' lorsqu'on rentre dans 'SQLite' avec la commande 'sqlite3'.  
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;On retourne dans le 'shell' lorsqu'on utilise la commande '.exit' ou '.quit' depuis SQLite.  
`Bash` = le language de programmation utilisé dans le shell de nos ordinateurs. C'est aussi un programme qui améliore le shell.  

`/` = **Includes** = **contient**  
`<username>` est à remplacer par son propre nom **!**  
`~` est un "tilde", il renvoie au dossier personnel.  
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;*Autrement dit, il remplace* `/home/<username>`  
Un fichier se termine par une extension, écrite après le point (.csv, .md, etc).  
Un dossier se termine par un slash ("/").  
`.` dans le shell renvoie au dossier actuel : le dossier dans lequel on est.  
`..` dans le shell renvoie au dossier parent du dossier actuel.  
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Traduction :  
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;`./` veut dire :  
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;`.` "le dossier actuel" `/` "contient"  
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;*Autrement dit, c'est où on est. (e.g.:* `ls` __=__ `ls ./`*)*  
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;`ls ../` *montrerait le contenu du dossier parent, qui incluerait le dossier dans lequel on est.*  
`parent` veut dire le dossier précédent = le dossier qui mène à notre position actuelle.  
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;e.g.: `~/prairie/` ici, on se trouve dans le dossier "prairie", qui est le dossier actuel. Le dossier "prairie" est situé dans le dossier parent : "~".  
>Plus exactement, `~` renvoie à "/home/\<username\>", donc le dossier parent est "\<username\>".

![Erreur dans le lien vers l'image : path_files_001](./images/path_files_001.png "home")
