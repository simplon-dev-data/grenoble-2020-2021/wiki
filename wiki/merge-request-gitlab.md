# A quoi sert une merge-request dans Gitlab ?

## TLDR

Une merge-request est une demande de reunion entre 2 branches d'une base commune.
ex : Branches A (master) version 1.0 --> Création de la branche B pour un test code sans dégradée la branche A.
Branche B fonction -> Merge-request de la branche B dans la branche A --> version 1.1.
Cela permet egalement de voir les changement,faire des corrections, évité les doublons,les suppressions de données par écrasement et permet de . 

## A Quoi sa sert ?

Une fois la nouvelle branche crée modifiez les fichiers et transférez-la vers GitLab, vous pouvez créer une demande de fusion (merge-request) .

La branche dans laquelle vous avez ajouté vos modifications est appelée branche source et la branche dans laquelle vous demandez de fusionner vos modifications est appelée branche cible.

La branche cible peut être la branche par défaut ou toute autre branche, selon vos besoin.

Lorsque vous démarrez une nouvelle demande de fusion, vous pouvez immédiatement inclure les options suivantes, ou les ajouter plus tard en cliquant sur le bouton Modifier sur la page de la demande de fusion en haut à droite:

-Attribuez la demande de fusion à un collègue pour examen. Avec GitLab Starter et les niveaux supérieurs, vous pouvez l'attribuer à plusieurs personnes à la fois.

-Définissez un jalon pour suivre les modifications urgentes.
-Ajoutez des libellés pour aider à contextualiser et à filtrer vos demandes de fusion au fil du temps.
-Exigez l'approbation de votre équipe.
-Fermez automatiquement les problèmes lorsqu'ils sont fusionnés.
-Activez l'option de suppression de la branche source lorsque la demande de fusion est acceptée pour garder votre référentiel propre.
-Activez l'option squash commits lorsque la demande de fusion est acceptée pour combiner tous les commits en un seul avant de fusionner, gardez ainsi un historique de commit propre dans votre référentiel.
-Définissez la demande de fusion comme brouillon pour éviter les fusions accidentelles avant qu'elle ne soit prête.

## Comment on fait ?

Après la création et modification de la branche sur gitlab ou après l'importation un bouton "create merge_requests" est visible en haut a droite. 

Il faut remplir les renseigements demandées par le logiciel :
- Description
- Assignee (Qui receveras la demande)
- Millestone (Jalon des problèmes)
- Labels (Marque pour retrouvée la question)

Et indiquée si l'on soihaite suprimée la branche après la merge

La demande sera enssuite envoyée à la personnes Assignee qui pourra validée.

## Références

Gitlab - 5. Les Merge Request | tutos fr (français)
https://www.youtube.com/watch?v=Gf7pKDE_1K4

https://docs.gitlab.com/ee/user/project/merge_requests/getting_started.html (anglais)

https://simplon-dev-data.gitlab.io/grenoble-2020-2021/wiki/ajouter-du-contenu/ (français)
