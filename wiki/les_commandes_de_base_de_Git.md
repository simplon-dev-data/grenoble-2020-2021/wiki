# les commandes de base Git

## Configuration de Git
 
Avant toute chose, configurons rapidement Git. Cela nous permettra de gagner du temps par la suite.
La première chose à faire est de définir notre identité : l’email et le nom d’utilisateur utilisé lorsque nous faisons un commit.

`git config --global user.name "<prénom> <nom>"`

`git config --global user.email "<adresse_mail>"`

## Création d’un dépôt Git
 Dans un flux de travail classique, lorsque l’on commence un nouveau projet, on créé alors un dépôt vide.

`git init`

## Créer un dépôt Git à partir d'un dépôt Gitlab existant

`git clone <gitlab-repository-url>`

## Statut du dépôt 
Voir l'état du répertoire de travail (les nouveaux fichiers créés ou modifiés)

`git status`

## Ajout de l'ensemble des fichiers modifiés à l'index (pas dans le repository directement )

`git add .`

## Ajout du fichier "mon_fichier.md" à l'index

`git add mon_fichier.md`


## Sauvegarder les modifications

`git commit -m "premier commit de mon projet"`

## Afficher l’historique des commits

`git log`

## Lister et créer des branches

`git branch `

## Changer de branche

`git checkout <nom-fichier>`

## Retourner dans la branche principale

`git checkout master`

## Fusionner une autre branche avec la principale

`git merge <nom-branche>`

## Connecter le repository local à celui de Gitlab

`git remote add origin <gitlab-repository-url>`

## Publier les changements sur Gitlab

`git push origin <nom-branche>`

## Récupérer les changements effectués (utile quand plusieurs personnes développent en même temps)

`git pull origin <nom-branche>`

# References

https://doc.ubuntu-fr.org/git
