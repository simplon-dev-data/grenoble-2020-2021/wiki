# Comment traiter les données manquantes ?

> La qualité des données est une des clés principales pour mener à bien un projet data.

## Résumé

Le problème de la gestion des données manquantes est un vaste sujet.
Les données manquantes ne peuvent pas être ignorées lors d'une analyse. 
Mais selon leur proportion et leur type, des solutions différentes vont être choisies.

La première chose à faire quand on est face à des données qui nous sont peu ou pas familières, c'est de regarder la "tête qu'elles ont".

## Qu'est ce qu'une "donnée manquante"

les données manquantes ou les valeurs manquantes se produisent lorsqu'aucune valeur de données n'est représentée pour une variable pour une observation donnée.

Les données manquantes sont courantes et peuvent avoir un effet  significatif sur l'[inférence](https://www.google.com/search?channel=fs&client=ubuntu&q=definition+inference),  les performances de prédiction ou tout autre utilisation faite avec les données.

## Les types de données manquantes:

> Il est important de comprendre les raisons pour lesquelles des données sont manquantes afin de bien utiliser l'ensemble des données. Si les valeurs sont manquantes de manière complètement aléatoire, l'échantillon de données peut encore être représentatif. Néanmoins, si les valeurs manquent de manière systématique, l'analyse peut être biaisée.

- [manquantes de manières complètement aléatoire](https://fr.wikipedia.org/wiki/Donn%C3%A9es_manquantes#Donn%C3%A9es_manquantes_de_mani%C3%A8re_compl%C3%A8tement_al%C3%A9atoire_(MMCA))
- [manquantes aléatoirement](https://fr.wikipedia.org/wiki/Donn%C3%A9es_manquantes#Donn%C3%A9es_manquantes_al%C3%A9atoirement)
- [manquantes par omissions prévisibles](https://fr.wikipedia.org/wiki/Donn%C3%A9es_manquantes#Manquant_par_omission_pr%C3%A9visible)


## Pourquoi j'ai des données manquantes ?

Tout dépend de la source des données, exemple de sources.

- saisie des données manuellement par des humains : il  ya de fortes chances pour que des erreurs se soient glissées dans la saisie, les sources d'erreurs sont multiples (formulaire d'un site web, saisie dans un tableur...)
- Si les données proviennent de capteurs (système de géolocalisation du smartphone, capteur de vitesse du véhicule, capteur de température...), il se peut que le capteur se dégrade au cours du temps et ne soit plus étalonné ou bien qu'il ne fonctionne plus (il ne transmet plus de données)
- Les données ont été perdues lors du transfert manuel d’une base de données existante
- Il y a eu une erreur de programmation
- etc...


## Techniques de traitement des données manquantes

Les données manquantes réduisent la représentativité de l'échantillon, ce qui peut fausser l'extrapolation des conclusions de l'étude, il existe trois approches principales pour gérer les données manquantes:

1- IMPUTATION : des valeurs viennent remplacer les données manquantes du jeu de données.

2- OMISSION   : les échantillons qui contiennent des données non valides sont rejetées pour le reste de l'analyse.

3- ANALYSE    : utiliser des méthodes sur lesquelles les données manquantes n'ont aucune incidence sur le résultat. 

## Savoir reconnaitre les données manquantes

Liste non exhaustives de cas que nous pouvons rencontrer
 
- Le cas le plus simple à identifier est le caractère vide ou l’espace pour les variables de type chaînes de caractères. Il est également possible d’avoir à faire à des « no data ».

- Dans le même type de cas de figure mais pour les variables numériques, on retrouve régulièrement les « 999 » et autres nombres volontairement incohérents.

- Les données atypiques ou aberrantes constituent également des valeurs manquante



## Références

https://fr.wikipedia.org/wiki/Donn%C3%A9es_manquantes

https://thinkr.fr/donnees-manquantes-causes-identification-et-imputation/

https://moncoachdata.com/blog/nettoyage-de-donnees-python/

https://perso.univ-rennes1.fr/valerie.monbet/doc/cours/IntroDM/Chapitre4.pdf




