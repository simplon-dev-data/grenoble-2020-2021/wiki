# Comment poser une question ?

Ce repository vous permet de **poser des questions** et de **répondre aux questions** des autres apprenants. Pour poser une question, il vous suffit de créer une **issue** en suivant ces étapes :

---

- Créer une issue

![Image créer une issue](static/tuto_issue_1.png)

---

- Choisir un titre explicite à la question, selectionner le template de question *Template_Question*, remplir le corps de la question en supprimant les commentaires

![Image ajouter des informations](static/tuto_issue_2.png)

---

- Ajouter les labels pertinents pour la question

![Image ajouter des labels](static/tuto_issue_3.png)

---

- Valider la question en cliquant sur *Submit issue*