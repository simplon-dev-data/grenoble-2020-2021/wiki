# 
# Comprendre les JOIN SQL, les utiliser
  
Comment faire un `JOIN` en SQL, que se passe-t-il lorsque on en fait un ?

# TL;DR

Un `JOIN` SQL permet de requêter une table en plus de la table principale.  
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;↳La vue créée à l'aide de la requête pourra ainsi contenir des données provenant de plusieurs tables.  
Il est possible d'enchaîner plusieurs `JOIN` à la suite pour rajouter d'autres tables.  
Il se fait avec une condition `ON` qui fusionne les lignes des tables lorsqu'elles vérifient la condition.

## Comment faire pour `JOIN` : la syntaxe

Pour faire un `JOIN` entre deux tables, il est nécessaire de définir :
- la table qu'on utilise, comme pour chaque requête SQL, avec la commande `FROM <table>`
- la table qu'on veut joindre, avec la commande `JOIN <table>`
- la condition qui va permettre la jointure, avec la commande `ON <condition>`  
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;↳ nous reviendrons sur cette condition dès la fin du premier exemple.

la commande `FROM` doit prendre sa place habituelle dans une requête SQL,  
et les autres commandes doivent la suivre directement.

## Les différents types de `JOIN`

### Les `JOIN` de base

Quatre jointures de bases existent en SQL :
- Le `INNER JOIN` (ou `JOIN`)
- Le `LEFT JOIN` (ou `LEFT OUTER JOIN`)
- Le `RIGHT JOIN` (ou `RIGHT OUTER JOIN`) - *impossible avec sqlite3*
- Le `FULL JOIN` (ou `FULL OUTER JOIN`) - *impossible avec sqlite3*

Pour comprendre les `JOIN` et leurs différences,  
il est plus simple de regarder d'abord comment le `FULL JOIN` fonctionne.

## Le cas le plus simple : le `FULL JOIN` de tables à valeurs uniques

Pour comprendre le `FULL JOIN`, construisons un exemple simple avec  
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;deux tables `table_A` et `table_B` d'une seule colonne chacune,  
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;respectivement `colonne_a` et `colonne_b` :

```SQL
CREATE TABLE table_A(colonne_a INT) ;
CREATE TABLE table_B(colonne_b INT) ;
```
Puis insérons quelques valeurs dans ces deux tables :

```SQL
INSERT INTO table_A(colonne_a)
    VALUES  (0),
            (1),
            (2),
            (3) ;
INSERT INTO table_B(colonne_b)
    VALUES  (0),
            (3),
            (4),
            (5) ;
```

Nous voilà avec deux tables, `table_A` et `table_B`, remplies d'un petit nombre d'`INTEGER` chacune,  
affichons leur contenu avec les requêtes :

```SQL
SELECT * FROM table_A ;
SELECT * FROM table_B ;
```
  
|`colonne_a`|
|-|
|0|
|1|
|2|
|3|

|`colonne_b`|
|-|
|0|
|3|
|4|
|5|

Faisons maintenant un `FULL JOIN` pour voir le résultat :

>Lorsque qu'on écrit le nom d'une colonne, il est possible de préciser de quelle table cette  
>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;colonne provient en utilisant un `.` et en suivant la syntaxe : `<table>.<colonne>`.</br>
>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Il est parfois nécessaire, **et toujours préférable** d'utiliser cette syntaxe lors de jointures.

```SQL
SELECT * 
FROM table_A
    FULL JOIN table_B
    ON table_A.colonne_a = table_B.colonne_b
;
```

|`colonne_a`|`colonne_b`|
|-|-|
|0|0|
|1||
|2||
|3|3|
||4|
||5|

Comme nous pouvons le constater, toutes les lignes de chaque table ont été réunies dans une seule vue.  
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;↳ le `FULL JOIN` permet de joindre la totalité des lignes de deux tables différentes ensemble.  
Elles n'ont pas été rassemblées n'importe comment, mais dans le respect de la condition définie grâce à `ON`  
En effet, lorsque `A.a` est égal à `B.b`, SQL a fusionné les lignes des deux tables en une seule.  
Les autres lignes ont été gardées séparées car elles ne respectaient pas la condition.



## Les autres `JOIN`

### Les `LEFT JOIN` et `RIGHT JOIN`

À partir de l'exemple précédent, nous pouvons voir facilement la différence entre un `FULL JOIN`  
et un `LEFT JOIN` à l'aide des tables créées `table_A` et `table_B` :

```SQL
SELECT *
FROM table_A
    LEFT JOIN table_B
    ON table_A.colonne_a = table_B.colonne_b
;
```

|`colonne_a`|`colonne_b`|
|-|-|
|0|0|
|1||
|2||
|3|3|

Nous constatons que seule la table "à gauche du join" (*the __left__ table*) a été conservée dans sa totalité.  
C'est le `LEFT JOIN`, il conserve toute la table de gauche, mais ne récupère les données de la  
table de droite que lorsqu'elles vérifient la condition `ON`.  

Pour le `RIGHT JOIN`, c'est tout simplement l'inverse :

```SQL
SELECT *
FROM table_A
    RIGHT JOIN table_B
    ON table_A.colonne_a = table_B.colonne_b
;
```

|`colonne_a`|`colonne_b`|
|-|-|
|0|0|
|3|3|
||4|
||5|

## Le `INNER JOIN` = `JOIN`

> `JOIN` est la syntaxe simplifiée de `INNER JOIN` : les deux sont identiques

Voyons son effet avec un exemple :

```SQL
SELECT *
FROM table_A
    JOIN table_B
    ON table_A.colonne_a = table_B.colonne_b
;
```

|`colonne_a`|`colonne_b`|
|-|-|
|0|0|
|3|3|

Nous remarquons que le `INNER JOIN` ne conserve que les lignes  
dont la condition `table_A.colonne_A = table_b.colonne_b` est vérifiée.

## Toutes les colonnes sont jointes lors d'un  `JOIN`

> On joint bien des <a style="font-size:20px">**tables**</a> ! (pas des colonnes)

Pour le constater, rajoutons d'abord une colonne à notre table `table_B` :

```SQL
ALTER TABLE table_B ADD COLUMN colonne_bb TEXT ;
UPDATE table_B SET colonne_bb = '00' WHERE colonne_b = 0 ;
UPDATE table_B SET colonne_bb = '33' WHERE colonne_b = 3 ;
UPDATE table_B SET colonne_bb = '44' WHERE colonne_b = 4 ;
UPDATE table_B SET colonne_bb = '55' WHERE colonne_b = 5 ;
```

```SQL
SELECT * FROM table_B ;
```

|`colonne_b`|`colonne_bb`|
|-|-|
|0|00|
|3|33|
|4|44|
|5|55|

Puis reprenons notre première requête :  

```SQL
SELECT *
FROM table_A
    FULL JOIN table_B
    ON table_A.colonne_a = table_B.colonne_b
;
```

|`colonne_a`|`colonne_b`|`colonne_bb`|
|-|-|-|
|0|0|00|
|1|||
|2|||
|3|3|33|
||4|44|
||5|55|

Les lignes de la table `table_B` restent les mêmes, elles sont simplement  
alignées avec celles de la table `table_A` lorsque la condition est remplie.  
Cette *vue* *(i.e.: une table virtuelle/temporaire)* représente le résultat entier  
de la fusion entre les deux tables, car nous avons utilisé un  `SELECT *` seul.</br>
Nous ne sommes cependant pas obligés de séléctionner toutes les colonnes :

```SQL
SELECT table_A.colonne_a, table_B.colonne_bb
FROM table_A
    FULL JOIN table_B
    ON table_A.colonne_a = table_B.colonne_b
;
```

|`colonne_a`|`colonne_bb`|
|-|-|
|0|00|
|1||
|2||
|3|33|
||44|
||55|

```SQL
SELECT table_B.colonne_bb
FROM table_A
    FULL JOIN table_B
    ON table_A.colonne_a = table_B.colonne_b
;
```

|`colonne_bb`|
|-|
|00|
||
||
|33|
|44|
|55|

Nous voyons en ne séléctionnant que la colonne `colonne_bb` que le `SELECT` a  
bien été fait sur la jointure des deux tables car il y a deux lignes avec des valeurs nulles.  
>Cela découle de l'ordre d'exécution des commandes d'une requête SQL. (voir l'ordre [ici](https://sqlbolt.com/lesson/select_queries_order_of_execution) par exemple)  
>Le `SELECT` (la sélection des colonnes) est executé après la sélection des tables.

## Le cas de valeurs multiples

Il est courant d'avoir dans des tables de données des valeurs  
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;qui ne sont pas uniques dans une ou plusieurs colonnes.  
Si un de ces colonnes est présente dans la condition `ON` le `JOIN` affichera chaque combinaison  
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;possible de valeurs respectant la condition.  
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Les lignes seront dupliquées dès que c'est nécessaire.

Voyons plutôt par l'exemple :

Créons deux tables simples et remplissons-les :

```SQL
CREATE TABLE A(
    a INT
) ;
CREATE TABLE B(
    b INT,
    bb TEXT
) ;
INSERT INTO A(a)
    VALUES  (0),
            (1),
            (2),
            (3) ;
INSERT INTO B(b, bb)
    VALUES  (1, 'spam'),
            (1, 'eggs'),
            (1, 'foo'),
            (1, 'bar') ;
SELECT * FROM A ;
SELECT * FROM B ;
```

|`a`|
|-|
|0|
|1|
|2|
|3|

|`b`|`bb`|
|-|-|
|1|spam|
|1|eggs|
|1|foo|
|1|bar|

Puis tentons un `FULL JOIN` pour voir le résultat :

```SQL
SELECT *
FROM A
    JOIN B
    ON A.a = B.b
;
```

|`a`|`b`|`bb`|
|-|-|-|
|0|||
|1|1|spam|
|1|1|eggs|
|1|1|foo|
|1|1|bar|
|2|||
|3|||

Nous pouvons constater que lorsque plusieurs possibilités existaient  
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;pour remplir la condition du `JOIN`, SQL a dupliqué la</br> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;valeur `1` de la colonne `a` autant de fois que nécessaire</br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;pour afficher chaque combinaison possible.
>Autrement SQL aurait du choisir arbitrairement l'une des valeurs `1` pour :  
>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;(`1`, `spam`, (`1`, `eggs`), (`1`, `foo`) ou (`1`, `bar`)</br>
>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;pour remplir la condition sans dupliquer de ligne

## Conclusion : modélisons !

Le `JOIN` est un outil puissant de SQL qui permet beaucoup de choses.  
Puisque son utilisation peut vite devenir labyrinthique, il vaut mieux faire des schémas  
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;pour arriver à s'y retrouver : des modèles de bases de données (c'est ce qu'on fait !)  
L'une des utilités de ces modèles est de s'assurer que les `JOIN` de valeurs multiples ne dupliqueront  
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;pas des valeurs censées rester uniques. C'est pour cela qu'on écrit les cardinalités entre les  
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;différentes tables (e.g.: `1/n`)</br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;(i.e.: pour s'assurer que la logique et le raisonnement sont bons avant de se compliquer la tâche avec du code.)
</br>
</br>
</br>
</br>
</br>