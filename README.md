# Wiki Simplon Dev Data Grenoble 2020/2021

> Base de connaissance collaborative

Posez vos questions dans les ["issues"](https://gitlab.com/simplon-dev-data/grenoble-2020-2021/wiki/-/issues),
et centralisez les réponses automatiquement dans ce wiki !

Le wiki est autogénéré et déployé via GitLab Pages à chaque
["merge request"](https://gitlab.com/simplon-dev-data/grenoble-2020-2021/wiki/-/merge_requests)
dans la branche `master` !

Réalisé avec [MkDocs](https://www.mkdocs.org).

## Installation

```bash
pipenv install -d
pipenv shell
mkdocs serve
```

Ouvrez un navigateur à l'URL [http://localhost:8000](http://localhost:8000).

## Contenu

Le contenu de la base de connaissance se trouve dans le dossier [wiki](./wiki).

Toutes les fiches doivent être rédigées avec un des langages suivants:

- [Markdown](https://www.markdownguide.org)
- [Jupyter Notebook](https://jupyter.org)

Une fois le contenu rédigé, il suffit d'ouvrir une
["merge request"](https://gitlab.com/simplon-dev-data/grenoble-2020-2021/wiki/-/merge_requests)
et d'attendre la fusion avec la branche `master` !

## Génération

```bash
mkdocs build
```
